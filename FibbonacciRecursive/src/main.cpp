/*
*	University of Southern Denmark
*	Data Structures and Algorithms
*
*	Project:		Fibonacci Series with recursion
*	Modulename:		main.cpp
*
*	Description:	Implements the Fibonacci series with the use of recursion
*
*	------------------------------------------------------
*	Change Log:
*
*	Date		ID		Change
*	YYMMDD
*
*	140905		NAL		Module Created
*
*	------------------------------------------------------
*/


/***************************** Include files *******************************/
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <fstream>

/*****************************    Defines    *******************************/
#ifdef _WIN32
	#define WINDOWS_OS 1;
#else
	#define WINDOWS_OS 0;
#endif
/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/


int Fibonacci(int n)
{
	if (n == 0)
		return 0;
	else if (n == 1)
		return 1;
	else
		return (Fibonacci(n - 1) + Fibonacci(n - 2));
}

int main(){
	bool Windows_OS = WINDOWS_OS;
	clock_t t;
	std::ofstream file;
	file.open("time.txt");

	int i = 0, c;
	
	for (int n = 40; n < 45; ){
		i = 0;
		t = clock();
		//std::cout << "Fibonacci series" << std::endl;

		for (c = 1; c <= n; c++)
		{
			std::cout << i << ":\t" << Fibonacci(i) << std::endl;
			Fibonacci(i);
			i++;
		}
		t = clock() - t;
		float time = ((float)t) / CLOCKS_PER_SEC;
		std::cout << "Terms: " << n << "\t Takes: " << t << " clicks \t(" << time << " seconds)." << std::endl;
		file << time << ",";
		n += 5;
	}
	file.close();

	if(Windows_OS){
		system("pause");
	}

	return 0;
}
