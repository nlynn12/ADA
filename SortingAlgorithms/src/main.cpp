/*
 *	Project:	Search and Sorting-algorithms
 *	Filename:	Source.cpp
 *	Author(s):	N.Lynnerup
 *	Date:		2013.11.22
 *	Features:	Sorts and searches in lists
 */

#include <iostream>

#include "DataList.hpp"
#include "SearchAlgorithms.hpp"
#include "SortingAlgorithms.hpp"

int main(){

	SortingAlgorithms myList;

	int menu;

	do{
	std::cout << "---------- Menu ----------"<< std::endl;
	std::cout << " 1:Input list from file" << std::endl;
	std::cout << " 2:Generate random list" << std::endl;
	std::cout << " 3:Sort list" << std::endl;
	std::cout << " 4:Search list" << std::endl;
	std::cout << " 5:Print list" << std::endl;
	std::cout << " 6:Exit" << std::endl;
	std::cout << "--------------------------" << std::endl;
	std::cin >> menu;
	std::cout << std::endl;

	switch (menu){
		case 1:
			myList.readFromFile();
			break;
		case 2:
			int length;
			std::cout<<"Enter list lenght: ";
			std::cin>>length;
			myList.GenerateList(length);
			break;
		case 3:
			if(!isGenerated){
				std::cout << "No list has been inputted nor generated yet!" << std::endl;
				std::cout << "Please do that before you continue!" << std::endl;
			}else{
				myList.SortListMenu();
			}
			break;
		case 4:
			if(!isGenerated){
				std::cout << "No list has been inputted nor generated yet!" << std::endl;
				std::cout << "Please do that before you continue!" << std::endl;
			}else{
				myList.SearchListMenu();
			}
			break;
		case 5:
			myList.PrintList();
			break;
		case 6:
			menu = 6;
	}

	}while(menu == 1 || menu == 2 || menu == 3 || menu == 4 || menu == 5);

	return 0;
}
