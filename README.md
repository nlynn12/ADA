ADA - Algorithms and Data Structures
====
University of Southern Denmark

###Programming Exercises
The projects above can be compiled by simply using cmake

```
$ cmake CMakeLists.txt
$ make
```
Executables are defined to be placed in the bin folder.

**Note** that the no external framework is needed. Only C++11 support.


### Course Description

######Knowledge
* Data structures (i.e. list, stack, queue, tree, binary search tree, AVL-tree, heap, graph).
* Algorithm principles (i.e. greedy, divide and conquer), dynamic programming.
* Complexity analysis of data structures and algorithms including recurrence.
* Problems and examples regarding data structures and algorithms.
* Implementation and language based constructions in an object oriented language.

######Skills
* Improved programming skills and experience in an object oriented language.

######Competences
* Implement above mentioned data structures and algorithms optimally with respect to time and space consumption.
