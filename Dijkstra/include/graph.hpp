/*
 *	University of Southern Denmark
 *	Data Structures and Algorithms
 *
 *	Project:		Dijkstra's Algorithm (shortest path)
 *	Module name:	graph.h
 *
 *	Description:	Implements Graph class and Dijkstra's algorithm
 *
 *	------------------------------------------------------
 *	Change Log:
 *
 *	Date		ID			Change
 *	YYMMDD
 *
 *	141030		JJB			Module Created
 *	141101		NAL			Modules: edge.h, edge.cpp, vertix.h, vertix.cpp removed
 *							- structs created instead.
 *
 *
 *	------------------------------------------------------
 */

#ifndef _GRAPH_H
#define _GRAPH_H

/***************************** Include files *******************************/
#include <unordered_map>
#include <string>
#include <vector>

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

struct vertex;

struct edge {
	float cost;
	struct vertex * destination;
};

struct vertex {
	struct vertex *path;
	bool visited;
	int distance;
	std::vector<struct edge*> adj;
	std::string cityName;
};

class graph {
private:
	struct vertex* MakeEmptyVertex(std::string);
	void MakeListofEdges(vertex*, std::vector<std::string>);
	struct edge* MakeEdge(vertex*, std::string);
public:
	void MakeVertex(std::string, std::vector<std::string>);
	void dijkstra(vertex *);
	void printPath(vertex *,int);
	void printDest(vertex *);
	std::unordered_map<std::string, vertex*> hashtable;
};

#endif
