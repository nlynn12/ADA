/*
 *	University of Southern Denmark
 *	Data Structures and Algorithms
 *
 *	Project:		AVL tree
 *	Module name:	avl_node.h
 *
 *	Description:	Holds the data type avl_node
 *
 *	------------------------------------------------------
 *	Change Log:
 *
 *	Date		ID		Change
 *	YYMMDD
 *
 *	140922		NAL		Module Created
 *
 *	------------------------------------------------------
 */

#ifndef _AVL_NODE_H
#define _AVL_NODE_H

struct avl_node {
	int data;
	int dataCounter = 0;
	struct avl_node *left;
	struct avl_node *right;
}*root;

#endif //_AVL_NODE_H
