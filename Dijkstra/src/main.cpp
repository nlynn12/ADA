/*
*	University of Southern Denmark
*	Data Structures and Algorithms
*
*	Project:		Dijkstra's Algorithm (shortest path)
*	Module name:	main.cpp
*
*	Description:	Implements Dijkstra's algorithm
*
*	------------------------------------------------------
*	Change Log:
*
*	Date		ID			Change
*	YYMMDD
*
*	141030		JJB			Module Created
*				NAL,JGJ		Read file functions added
*	141031		NAL,JGJ		Read file functions revised
*							- Converted to std::vector<std::string>
*	141101		NAL			Added support for multiple OS
*	141102		JJB			User interface added to main
*
*	------------------------------------------------------
*/

/***************************** Include files *******************************/
#include <iostream>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <algorithm>
#include <unordered_map>
#include <chrono>
#include "graph.hpp"
/*****************************    Defines    *******************************/
#ifdef _WIN32
#define WINDOWS_OS 1;
#else
#define WINDOWS_OS 0;
#endif

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

std::string* readDataFromFile() {
	// 1.1 Count number of lines
	std::ifstream file("routes.txt");
	std::string line;
	int number_of_lines = 0;
	while (std::getline(file, line)) {
		number_of_lines++;
	}
	file.close();

	// 1.2 Read line into string and store in array
	std::string *complete_list_from_file = new std::string[number_of_lines + 2];

	// 1.2.1 Convert integer to string
	std::stringstream ss;
	ss << number_of_lines;
	std::string str_size = ss.str();
	complete_list_from_file[0] = str_size;

	file.open("routes.txt");
	if (file.is_open()) {
		int i = 1;
		while (std::getline(file, line)) {
			complete_list_from_file[i] = line;
			i++;
		}
	}
	file.close();
	return complete_list_from_file;
}

std::vector<std::string> createSubstring(std::string stringArray[]) {
	// 1.3 Create a substring
	std::istringstream ss(stringArray[1]);
	std::string line;

	int number_of_elements = std::count(stringArray[1].begin(),
		stringArray[1].end(), ',');
	int size = number_of_elements + 3;
	std::string *substring = new std::string[size]; //2extra + 1 element for size + escape character

	//1.3.1 Character array of characters unwanted in the string array
	char chars[] = "{ }";

	int i = 0;
	while (std::getline(ss, line, ',')) {
		substring[i] = line;
		for (unsigned int j = 0; j < strlen(chars); ++j) {
			substring[i].erase(
				std::remove(substring[i].begin(), substring[i].end(),
				chars[j]), substring[i].end());
		}
		i++;
	}

	std::vector<std::string> v(substring, substring + size - 1);

	return v;
}

int main() {
	bool Windows_OS = WINDOWS_OS;
	int choice;
	std::string from;
	std::string to;

	//Chrono Time Setup
	static auto endTimePoint = std::chrono::system_clock::now();
	static auto startTimePoint = std::chrono::system_clock::now();
	static auto diffTimePoint = endTimePoint - startTimePoint;
	double timeDifference;

	//Initial Program to load file and generate Datastructure
	std::cout<<"Generating Datastructure..."<<std::endl;
	//Create instance of Graph
	graph myGraph;

	//Read the File
	std::string *data = readDataFromFile();
	int number_of_cities;
	std::istringstream ss(data[0]);
	ss >> number_of_cities;

	//Run through File and add to Graph
	for (int j = 0; j < number_of_cities ; j++) {
		std::vector<std::string> temp = createSubstring(&data[j]);
		myGraph.MakeVertex(temp[0], temp);
	}

	// User interface: Choose To print possible destinations or Find route to destination
	while(true)
	{
		std::cout<<"1: Cities you can reach from a given city"<<std::endl;
		std::cout<<"2: Route from city 1 to city 2"<<std::endl;
		std::cout<<"3. Cities you cant go to"<<std::endl;
		std::cout<<"4. Timing"<<std::endl;
		
		std::cin>>choice;
		switch(choice)
		{
		case 1:
			std::cout<<"Name of city: ";
			std::cin>>from;
			//Run dijstra
			myGraph.dijkstra(myGraph.hashtable[from]);
			//Print destinations
			myGraph.printDest(myGraph.hashtable[from]);
			break;
		case 2:
			std::cout<<"From City: ";
			std::cin>>from;
			std::cout<<"To City: ";
			std::cin>>to;
			std::cout<<std::endl;
			//Run dijstra
			myGraph.dijkstra(myGraph.hashtable[from]);
			//Print shortest path
			myGraph.printPath(myGraph.hashtable[to],0);
			std::cout<<std::endl;
			std::cout<<std::endl;
			std::cout<<"Price: "<<myGraph.hashtable[to]->distance<<std::endl;
			std::cout<<std::endl;
			
			break;
		case 3:
			
			for(auto kv : myGraph.hashtable) 
			{
			if(kv.second->path == nullptr)
			{
				std::cout<<kv.second->cityName<<",";
			}
			}
			
			break;
		case 4:
			//This willrun dijstra's for all vertices and print the meantime
			std::cout<<"Runing Dejkstra's Algoritm for all vertices.."<<std::endl;
			startTimePoint = std::chrono::system_clock::now();
			for(auto kv : myGraph.hashtable) 
			{
			
				myGraph.dijkstra(kv.second);
			}
			endTimePoint = std::chrono::system_clock::now();
			diffTimePoint = endTimePoint - startTimePoint;
			timeDifference = std::chrono::duration<double,std::milli>(diffTimePoint).count();
			std::cout<<"Average time: "<<timeDifference/(myGraph.hashtable.size())<<" milliseconds"<<std::endl;
			std::cout<<std::endl;
			break;

		default:
			std::cout<<myGraph.hashtable.size()<<std::endl;
			std::cout<<"Wrong Choice"<<std::endl;
			break;
		}
	}

	if (Windows_OS) {
		system("pause");
	}
	return 0;
}
