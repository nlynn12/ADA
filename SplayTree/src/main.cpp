/*
 *	University of Southern Denmark
 *	Data Structures and Algorithms
 *
 *	Project:		Splay tree
 *	Module name:	main.cpp
 *
 *	Description:	Implements a splay tree
 *
 *	------------------------------------------------------
 *	Change Log:
 *
 *	Date		ID		Change
 *	YYMMDD
 *
 *	140924		NAL		Module Created
 *
 *	------------------------------------------------------
 */

/***************************** Include files *******************************/
#include <iostream>
#include <cstdlib>
#include <chrono>
#include <fstream>

#include "splay_tree.hpp"
#include "random.hpp"
/*****************************    Defines    *******************************/
#ifdef _WIN32
#define WINDOWS_OS 1;
#else
#define WINDOWS_OS 0;
#endif

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

int main() {
	bool Windows_OS = WINDOWS_OS;
	int menu, value;
	splayTree tree;

	static auto endTimePoint = std::chrono::system_clock::now();
	static auto startTimePoint = std::chrono::system_clock::now();
	static auto diffTimePoint = endTimePoint - startTimePoint;

	double timeDifferenceMs;

	std::ofstream file;
	file.open("time_asc.txt");

	while (1) {
		std::cout << std::endl;
		std::cout << "----------------------------" << std::endl;
		std::cout << "       Splay Tree Menu" << std::endl;
		std::cout << "----------------------------" << std::endl;
		std::cout << "1. Insert element" << std::endl;
		std::cout << "2. Display splay tree" << std::endl;
		std::cout << "3. Search splay tree" << std::endl;
		std::cout << "4. Exit" << std::endl;
		std::cout << "\nEnter choice: ";
		std::cin >> menu;

		switch (menu) {
		case 1:
			int submenu;
			std::cout << "\n----------------------------" << std::endl;
			std::cout << "       Insert Submenu" << std::endl;
			std::cout << "----------------------------" << std::endl;
			std::cout << "1. Insert single element" << std::endl;
			std::cout << "2. Insert multiple elements (ascending)" << std::endl;
			std::cout << "3. Insert multiple elements (descending)" << std::endl;
			std::cout << "4. Insert random numbers" << std::endl;
			std::cout << "5. Cancel" << std::endl;
			std::cout << "\nEnter choice: ";
			std::cin >> submenu;

			switch (submenu) {
			case 1:
				std::cout << "Enter value: ";
				std::cin >> value;
				root = tree.insert(root, value);
				break;
			case 2:
				for (int j = 0; j <= 1000; j+=5) {
					splayTree tree;
					startTimePoint = std::chrono::system_clock::now();
					for (int i=1; i <= j; i++){
						root = tree.insert(root, i);
					}
					endTimePoint = std::chrono::system_clock::now();
					diffTimePoint = endTimePoint - startTimePoint;

					timeDifferenceMs = std::chrono::duration<double,std::micro>(diffTimePoint).count();
					file << j << "," << timeDifferenceMs << std::endl;
				}
				break;
			case 3:
				for (int j = 0; j <= 1000; j+=5) {
					splayTree tree;
					startTimePoint = std::chrono::system_clock::now();
					for (int i=j; i >= 0; i--){
						root = tree.insert(root, i);
					}
					endTimePoint = std::chrono::system_clock::now();
					diffTimePoint = endTimePoint - startTimePoint;

					timeDifferenceMs = std::chrono::duration<double,std::micro>(diffTimePoint).count();
					file << j << "," << timeDifferenceMs << std::endl;
				}
				break;
			case 4:
				for (int j = 0; j <= 1000; j+=5) {
					splayTree tree;
					startTimePoint = std::chrono::system_clock::now();
					for (int i=1; i <= j; i++){
						root = tree.insert(root, list[i]);
					}
					endTimePoint = std::chrono::system_clock::now();
					diffTimePoint = endTimePoint - startTimePoint;

					timeDifferenceMs = std::chrono::duration<double,std::micro>(diffTimePoint).count();
					file << j << "," << timeDifferenceMs << std::endl;
				}
				break;
			case 5:
				break;
			}
			break;
		case 2:
			if (root == NULL) {
				std::cout << "The tree is empty" << std::endl;
				continue;
			}
			tree.display(root, 1);
			break;
		case 3:
			std::cout << "Enter value: ";
			std::cin >> value;
			root = tree.contains(root, value);
			tree.display(root, 1);
			break;
		case 4:
			std::cout << "Exiting..." << std::endl;
			if (Windows_OS) {
				system("pause");
			}
			exit(1);
			break;
		default:
			std::cout << "Error in choice of menu" << std::endl;
		}

	}

	if (Windows_OS) {
		system("pause");
	}
	return 0;
}
