/*
 *	Project:	Search and Sorting-algorithms
 *	Filename:	SearchAlgorithms.h
 *	Author(s):	N.Lynnerup
 *	Date:		2013.11.22
 *	Features:	Holds inherited class SearchAlgorithms
 *				-	Inherits virtual from base class, DataList, because of the ambiguity that arises
 *					when both SortingAlgorithms and SearchAlgorithms classes inherits from base class,
 *					DataList, and SortingAlgorithms class at the same time inherits from SearchAlgorithms.
 *					This problem is refered to as "The Diamond Problem", see:
 *					http://en.wikipedia.org/wiki/Diamond_problem#The_diamond_problem
 */

#ifndef SEARCH_ALGORITHMS
#define SEARCH_ALGORITHMS

#include "DataList.hpp"

class SearchAlgorithms : virtual public DataList{
	private:
		int numSearch;
	public:
		void SearchListMenu();
		void secsearch(int sNum);
		void binsearch(int sNum);
};


void SearchAlgorithms::SearchListMenu(){
	int submenuSearch;

	std::cout << "---------- Search ----------"<< std::endl;
	std::cout << " 1:Sequence Search" << std::endl;
	std::cout << " 2:Binary Search" << std::endl;
	std::cout << "----------------------------" << std::endl;
	std::cin >> submenuSearch;
	std::cout << std::endl;

	switch (submenuSearch){
		case 1:
			std::cout << "Please input the number you're looking for: ";
			std::cin >> numSearch;
			secsearch(numSearch);
			break;
		case 2:
			// in binSearch(); check if list has been sorted, if not, sort list before searching!
			if(!isSorted()){
				std::cout << "The list you tried to search in was unsorted!" << std::endl;
				std::cout << "It has now been sorted, which took " << ite << " iterations" << std::endl;
				//static_cast<SortingAlgorithms*>(this)->insertionSort();
				std::cout<<"It took " <<ite <<' ' <<"iterations" <<std::endl;
			}
			std::cout << "Please input the number you're looking for: ";
			std::cin >> numSearch;
			binsearch(numSearch);
			break;
	}
}

void SearchAlgorithms::secsearch(int sNum){
	position = -1;

	for(int i = 0; i<listSize; i++){
		if(list[i]==sNum){
			std::cout << "Number found!" << std::endl;
			position = i;
			break;
		}
	}

	if(position==-1){
		std::cout << "No such number in list" << std::endl;
	}
}

void SearchAlgorithms::binsearch(int sNum){
	int bottom = 0;
	int top = listSize-1;
	int middle = 0;

	while(top>bottom){
		middle = (top+bottom)/2;
		if(sNum==list[middle]){
			std::cout << "Number found!" << std::endl;
			break;
		}
		else if(sNum>list[middle]){
			bottom = middle+1;
		}
		else{
			top = middle;
		}
	}

	if(top==-1)
		std::cout << "The list is empty" << std::endl;
	else if(top <= bottom)
		std::cout << "No such number in list" << std::endl;
	else
		position = top;
}

#endif //SEARCH_ALGORITHMS
