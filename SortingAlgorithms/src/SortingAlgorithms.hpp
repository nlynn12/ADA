/*
 *	Project:	Search and Sorting-algorithms
 *	Filename:	SortingAlgorithms.h
 *	Author(s):	N.Lynnerup
 *	Date:		2013.11.22
 *	Features:	Holds inherited class SortingAlgorithms
 *				-	Inherits virtual from base class, DataList, because of the ambiguity that arises
 *					when both SortingAlgorithms and SearchAlgorithms classes inherits from base class,
 *					DataList, and SortingAlgorithms class at the same time inherits from SearchAlgorithms.
 *					This problem is refered to as "The Diamond Problem", see:
 *					http://en.wikipedia.org/wiki/Diamond_problem#The_diamond_problem
 */

#ifndef SORTING_ALGORITHMS
#define SORTING_ALGORITHMS

#include "DataList.hpp"

class SortingAlgorithms : virtual public DataList, public SearchAlgorithms{
	public:
		SortingAlgorithms();
		void SortListMenu();
		void bubblesortList();
		void selectionSort();
		void insertionSort();
};

/* Constructor */
SortingAlgorithms::SortingAlgorithms() : DataList(){};	// <-- is this really nescesary?

/* Menu of sort options */
void SortingAlgorithms::SortListMenu(){
	int submenuSort;

	std::cout << "---------- Sort ----------"<< std::endl;
	std::cout << " 1:Bubblesort" << std::endl;
	std::cout << " 2:Selectionsort" << std::endl;
	std::cout << " 3:Insertionsort" << std::endl;
	std::cout << "--------------------------" << std::endl;
	std::cin >> submenuSort;
	std::cout << std::endl;

	switch (submenuSort){
		case 1:
			bubblesortList();
			break;
		case 2:
			selectionSort();
			break;
		case 3:
			insertionSort();
			break;
	}
}

/* Bubble Sort */
void SortingAlgorithms::bubblesortList(){
	int swapchecker = 1, k = 1;
	ite = 0;

	while(swapchecker!=0){
		swapchecker = 0;
		for(int i=0;i<listSize-k;i++){
			ite++;
			if(list[i]>list[i+1]){
				std::swap(list[i],list[i+1]);
				swapchecker++;
			}
		}
		k++;
	}

	std::cout << "It took: " << ite << " iterations. And the complexivity is: " << (double)ite/listSize << std::endl;

	/*	Source: http://www.cprogramming.com/tutorial/computersciencetheory/sorting1.html
	 *
	 *	n = listSize
	 *
	 *	Notice that this will always loop n times from 0 to n, so the order of this algorithm is O(n^2).
	 *	This is both the best and worst case scenario because the code contains no way of determining if
	 *	the array is already in order.
	 */
	  

}

/* Selection Sort */
void SortingAlgorithms::selectionSort(){
	int lownum;
	ite=0;
    for(int j=0;j<listSize-1;j++){
		lownum=j;
		for(int i=j;i<listSize;i++){
			if(list[i]<list[lownum])
				lownum=i;
                ite++;
		}
        if(lownum!=j)
                std::swap(list[lownum],list[j]);
	}

	std::cout << "It took: " << ite << " iterations. And the complexivity is: " << (double)ite/listSize << std::endl;

	/*	Source: http://www.cprogramming.com/tutorial/computersciencetheory/sorting2.html
	 *
	 *	n = listSize
	 *	x = j
	 *
	 *	The first loop goes from 0 to n, and the second loop goes from x to n,
	 *	so it goes from 0 to n, then from 1 to n, then from 2 to n and so on.
	 *	The multiplication works out so that the efficiency is n*(n/2), though the order is still O(n^2).
	 */
}

/* Insertion Sort */
void SortingAlgorithms::insertionSort()
{
	int temp=0;
	ite=0;

	for(int i=1;i<listSize;++i)
	{
		if(list[i]<list[i-1])
		{
			ite++;
			temp=list[i];
			for(int j=i;j>0;--j)
			{
				if(temp<list[j-1]&&j-1>0)
				{
					list[j]=list[j-1];
				}
				else if(j-1==0&&temp<list[j-1])
				{
					list[j]=list[j-1];
					list[j-1]=temp;
					break;
				}
				else
				{
					list[j]=temp;
					break;
				}
			}
		}
	}
	std::cout << "It took: " << ite << " iterations. And the complexivity is: " << (double)ite/listSize << std::endl;
}

#endif //SORTING_ALGORITHMS
