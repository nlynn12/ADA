/*
 *	University of Southern Denmark
 *	Data Structures and Algorithms
 *
 *	Project:		Dijkstra's Algorithm (shortest path)
 *	Module name:	graph.cpp
 *
 *	Description:	Implements Graph class and Dijkstra's algorithm
 *
 *	------------------------------------------------------
 *	Change Log:
 *
 *	Date		ID			Change
 *	YYMMDD
 *
 *	141030		JJB			Module Created
 *	141031		JJB,NAL		Module revised, errors removed:
 *							- Hashtable insert (emplace removed)
 *	141101		NAL			Module "beautified"
 *							- Constructor and destructor removed (using default)
 *							- vertix renamed to vertex
 *	141101		JJB			Dijkstra function was rewritten and now works
 *	141101		JJB			Added function to display all possible locations 
 *	141102		JJB			Updated dijkstra. Now has minimum runtime
 *	141105		JJB			Corrected dijstra. Now finds correct minimum path.
 *
 *
 *	------------------------------------------------------
 */

/***************************** Include files *******************************/
#include <iostream>
#include <stdlib.h>
#include <limits>
#include <unordered_map>

#include "graph.hpp"

/*****************************    Defines    *******************************/
#define infinite std::numeric_limits<int>::max()

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

//This Function Makes the vertex: Arguements are name of the city and; it's list of cities
//It will create a vertex with name of city and a List of Edges (Pointers to other cities)
//It will call the function MakeEmptyVertix if city is not present
void graph::MakeVertex(std::string cityName, std::vector<std::string> Adjen) {
	vertex *temp = hashtable[cityName];	//Check if Hashtable is containing the city/vertex
	if (temp == nullptr) {
		temp = MakeEmptyVertex(cityName);				//If not: create it
	}
	MakeListofEdges(temp, Adjen);
}

//This Function will create an "empty" vertex containing only the Name of the City!.
//It takes a string with name of city and return a pointer to the vertex-object.
vertex* graph::MakeEmptyVertex(std::string cityName) {
	vertex *temp = new vertex;
	temp->cityName = cityName;	//Create new instance of vertex and put in name
	hashtable[cityName] = temp;
	return temp;									//return pointer to object
}

//This function will create a list of edges in the object (vertex) that it is given.
//It calls the function MakeEdge() for evert city in the vector "Adjen".
//It will call the function MakeemptyVertix if city at hand is not already present.
void graph::MakeListofEdges(vertex* city, std::vector<std::string> Adjen) {
	int j = 0;
	for (int i = 1; i < Adjen.size() - 1; i += 2)//For the number of cities in the list create an edge
			{
		vertex *temp = hashtable[Adjen[i]];	//Check if Hashtable is containing the city/vertex
		if (temp == nullptr) {
			temp = MakeEmptyVertex(Adjen[i]);				//If not: create it
		}

		city->adj.push_back(MakeEdge(temp, Adjen[i + 1]));
		//city->cities[j] = MakeEdge(temp, Adjen[i+1]);	//Call the make edge function
		j++;
	}
}

//This function will create an edge with a pointer to a vertex and a price
edge* graph::MakeEdge(vertex* city, std::string price)//This method will create a new edge and put in the needed information
		{
	edge *temp = new edge;
	temp->destination = city;
	temp->cost = std::stoi(price);

	return temp;
}

void graph::dijkstra(vertex * s) {
	
	for(auto& kv : hashtable)
	{
		kv.second->distance = infinite;
		kv.second->visited = false;
		kv.second->path = nullptr;
	}

	s->distance = 0;
	vertex *curr = s;
	vertex * sudv = new vertex; //sudv = smallest unknow distance vertex 
	
	while(!curr->visited)
	{
		int min = infinite;
		
		curr->visited = true;
		
		
		for(unsigned int i=0; i != curr->adj.size(); i++)
		{
			
			if (!curr->adj[i]->destination->visited)
			{
				int cvw = curr->adj[i]->cost;

				if( curr->distance + cvw < curr->adj[i]->destination->distance)
				{
					//Update curr
					curr->adj[i]->destination->distance = curr->distance +cvw;
					curr->adj[i]->destination->path = curr;
					
				}
			}
		}
		
		//Update smallest unknow distance vertex 
				for(auto kv : hashtable) 
				{
				if(min > kv.second->distance && !kv.second->visited)
				{
					min = kv.second->distance;
					sudv = kv.second;
				}
				}
		//Update curr to be smallest unknow distance vertex
		curr = sudv;
	}
}

void graph::printPath(vertex *curr, int level) {
	if (curr->path != nullptr) {
		printPath(curr->path, level + 1);
		//std::cout<<curr->cityName;
		std::cout << " -> ";
	}
	//if (level==0){std::cout<<"There is no route to: ";}
	std::cout << curr->cityName;
}

void graph::printDest(vertex *v)
	{
		int i=0;
		for(auto kv : hashtable) 
		{
			if(! (kv.second->path == nullptr) )
			{
				std::cout<<kv.second->cityName<<",";
				i++;
			}
		}
		std::cout<<" Number of cities you can go to: "<<i<<std::endl;
	}
