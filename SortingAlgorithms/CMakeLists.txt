cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)

## Init the project
project(SortingAlgorithms LANGUAGES C CXX)
add_definitions(-std=c++11)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

include_directories(include/)

add_executable(${PROJECT_NAME}  src/main.cpp
                                src/DataList.hpp
                                src/SearchAlgorithms.hpp
                                src/SortingAlgorithms.hpp
                                )

set(CMAKE_BUILD_TYPE Release)
