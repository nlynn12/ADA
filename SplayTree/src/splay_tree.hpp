/*
 *	University of Southern Denmark
 *	Data Structures and Algorithms
 *
 *	Project:		Splay tree
 *	Module name:	splay_tree.h
 *
 *	Description:	Implements a splay tree
 *
 *	------------------------------------------------------
 *	Change Log:
 *
 *	Date		ID		Change
 *	YYMMDD
 *
 *	140924		NAL		Module Created
 *
 *	------------------------------------------------------
 */

#ifndef _AVL_TREE_H
#define _AVL_TREE_H

#include <stddef.h>

struct splay_node {
	int data;
	int dataCounter;
	splay_node *left;
	splay_node *right;
}*root;


class splayTree {
private:
	splay_node *RR_rotate(splay_node *);
	splay_node *LL_rotate(splay_node *);
public:
	splay_node *splay(splay_node *, int);
	splay_node *contains(splay_node *, int);
	splay_node *insert(splay_node *, int);
	void display(splay_node *, int );
};



// A utility function to right rotate subtree rooted in y
 splay_node *splayTree::RR_rotate(splay_node *x){
	splay_node *y = x->left;
	x->left = y->right;
	y->right = x;
	return y;
 }

 //A utility function to left rotate subtree rooted in y
 splay_node *splayTree::LL_rotate(splay_node *x){
	splay_node *y = x->right;
	x->right = y->left;
	y->left = x;
	return y;
 }

 //
 splay_node *splayTree::splay(splay_node *root, int data){
	 // Base case: root is null or data is present in root
	 if (root == NULL || root->data == data) return root;

	 // Data lies in the left subtree
	 if (data < root->data){
		// If node is not present
		if (root->left == NULL) return root;
		
		// Zig-Zig (left left rotation)
		if (root->left->data > data){
			// 1. Bring node up to be the root of left-left subtree
			root->left->left = splay(root->left->left, data);
			// 2. Do right rotation for root
			root = RR_rotate(root);
		// Zig-Zag (left right rotation)
		} else if (root->left->data < data){
			// 1. Bring data up to be the root of left-right subtree
			root->left->right = splay(root->left->right, data);
			// 2. Do left rotation for root
			if (root->left->right != NULL) root->left = LL_rotate(root->left);
		}

		// 3. Do final rotation
		return(root->left == NULL) ? root : RR_rotate(root);
	
	// Data lies in the right subtree
	}else{ 
		// If node is not present
		if (root->right == NULL) return root;

		// Zag-Zig (right left rotation)
		if (root->right->data > data){
			// 1. Bring data up to be the root of right-left subtree
			root->right->left = splay(root->right->left, data);
			// 2. Do right rotation for root
			if (root->right->left != NULL) root->right = RR_rotate(root->right);
		}
		// Zag-Zag (right-right rotation)
		else if (root->right->data < data){
			// 1. Bring data up to be the root of right right subtree
			root->right->right = splay(root->right->right, data);
			// 2. Do right rotation for root
			root = LL_rotate(root);
		}
		// 3. Do final rotation
		return(root->right == NULL) ? root : LL_rotate(root);
	}
 }
 
 //
 splay_node *splayTree::contains(splay_node *root, int data){
	 return splay(root, data);
 }

 //
 splay_node *splayTree::insert(splay_node *root, int data){
	 if (root == NULL){
		root = new splay_node;
		root->data = data;
		root->left = NULL;
		root->right = NULL;
		root->dataCounter++;
		return root;
	 }
	 else if (data < root->data){
		 root->left = insert(root->left, data);
	 }
	 else if (data > root->data){
		 root->right = insert(root->right, data);
	 }
	 else if (data == root->data){
		 root->dataCounter++;
	 }
	 return root;
 }

 //
 void splayTree::display(splay_node *x, int level){
	 if (x != NULL){
		 display(x->right, level + 1);
		 std::cout << std::endl;
		 if (x == root){
			 std::cout << "Root -> ";
		 }
		 for (int i = 0; i< level && x != root; i++){
			 std::cout << "        ";
		 }
		 std::cout << x->data;
		 display(x->left, level + 1);
	 }
 }


#endif //_AVL_TREE_H
