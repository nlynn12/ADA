/*
 *	Project:	Search and Sorting-algorithms
 *	Filename:	DataList.h
 *	Author(s):	N.Lynnerup
 *	Date:		2013.11.22
 *	Features:	Holds base class
 *				-	Base class' functionality: Read data from file, generate random list, print list, etc.
 */

#ifndef DATALIST
#define DATALIST

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>

bool isGenerated = false;		// Declared as global variable, how to avoid this?

class DataList {
	protected:
		int* list;
		std::string filename;
	public:
		DataList(){};
		std::string setFileName();
		void readFromFile();
		void GenerateList(int);
		void PrintList();
		int GetData(int);
		bool isSorted();
		int position,listSize,ite;
};


std::string DataList::setFileName(){
	std::cout << "Write filename: ";
	std::cin >> filename;
	filename.append(".txt");
	return filename;
}

void DataList::readFromFile(){
	int number;
	std::vector<int> listVec;

	std::ifstream myfile(setFileName());
	if (myfile.is_open()){
		while (!myfile.eof()){
			myfile >> number;
			listVec.push_back(number);
			std::cout << number << std::endl;
		}
		myfile.close();
	}
	else{
		std::cout << "No such file in directory";
	}
	std::cout << std::endl;

	listSize = listVec.size();
	list = new int[listSize];				// Create C-style array
	for (int i = 0; i<listSize; ++i){
		list[i] = listVec[i];				// Copy std::vector into C-style array
	}

	isGenerated = true;
}

/* Generates a list of length size with random numbers */
void DataList::GenerateList(int size){
	listSize = size;

	srand((unsigned)time(0));

	list = new int[listSize];

	for (int i = 0; i<listSize; i++){
		list[i] = rand() % 100;
	}

	isGenerated = true;

}

void DataList::PrintList(){
	for (int i = 0; i<listSize; ++i){
		std::cout << list[i] << " ";
	}
}

int DataList::GetData(int i){
	return list[i];
}

bool DataList::isSorted()
{
	for (int i = 0; i<listSize; ++i)
	{
		if (list[i]>list[i + 1] && i + 1 < listSize)
		{
			return false;
		}
	}
	return true;
};

#endif //DATALIST