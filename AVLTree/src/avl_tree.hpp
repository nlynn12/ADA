/*
 *	University of Southern Denmark
 *	Data Structures and Algorithms
 *
 *	Project:		AVL tree
 *	Module name:	avl_tree.h
 *
 *	Description:	Implements an AVL tree
 *
 *	------------------------------------------------------
 *	Change Log:
 *
 *	Date		ID		Change
 *	YYMMDD
 *
 *	140922		NAL		Module Created
 *						Class created with functions:
 *						- balance, difference, height
 *	140923		NAL		Functions added to class:
 *						- LL,RR,LR,RL rotations, display and search
 *
 *	------------------------------------------------------
 */

#ifndef _AVL_TREE_H
#define _AVL_TREE_H

#include <iomanip>
#include <stddef.h>
#include <algorithm>
#include "avl_node.hpp"


class avlTree {
private:
	int height(avl_node *);
	int difference(avl_node *);
	avl_node *LL_rotate(avl_node *);
	avl_node *LR_rotate(avl_node *);
	avl_node *RR_rotate(avl_node *);
	avl_node *RL_rotate(avl_node *);
	avl_node *balance(avl_node *);
public:
	avlTree(){root = NULL;}
	avl_node *insert(avl_node *, int );
	void displayAVLtree(avl_node *, int );
	int searchAVLtree( int );
};

/*
 * Compute height of AVL-tree
 */
int avlTree::height(avl_node *x){
	int tree_height = 0;

	if(x != NULL){
		int left_height = height(x->left);
		int right_height = height(x->right);
		int max_height = std::max(left_height,right_height);
		tree_height = max_height + 1;
	}

	return tree_height;
}

/*
 * Compute height difference with <node> as "root"
 */
int avlTree::difference(avl_node *x){
	int left_height = height(x->left);
	int right_height = height(x->right);
	int temp_balance_factor = left_height - right_height;
	return temp_balance_factor;
}

/*
 * Single Left Rotation
 */
avl_node *avlTree::LL_rotate(avl_node *x){
	avl_node *y = x->left;
	x->left = y->right;
	y->right = x;
	return y;
}

/*
 * Single Right Rotation
 */
avl_node *avlTree::RR_rotate(avl_node *x){
	avl_node *y = x->right;
	x->right = y->left;
	y->left = x;
	return y;
}

/*
 * Double Left Rotation (Left-Right Rotation)
 */
avl_node *avlTree::LR_rotate(avl_node *x){
	avl_node *y;
	y = x->left;
	x->left = RR_rotate(y);
	return LL_rotate(x);
}

/*
 * Double Right Rotation (Right-Left Rotation)
 */
avl_node *avlTree::RL_rotate(avl_node *x){
	avl_node *y;
	y = x->right;
	x->right = LL_rotate(y);
	return RR_rotate(x);
}

/*
 * Inserts element into AVL tree
 */
avl_node *avlTree::insert(avl_node *root, int value){
	if (root == NULL){
		root = new avl_node;
		root->data = value;
		root->left = NULL;
		root->right = NULL;
		root->dataCounter++;
		return root;
	}else if(value < root->data){
		root->left = insert(root->left, value);
		root = balance(root);
	}else if(value > root->data){
		root->right = insert(root->right, value);
		root = balance(root);
	}else if(value == root->data){
		root->dataCounter++;
	}
	return root;
}

/*
 * Performs rotations on AVL tree
 */
avl_node *avlTree::balance(avl_node *node){
	// Compute difference in height to determine the balance factor
	int balance_factor = difference(node);

	if(balance_factor > 1){ 				// Check if tree is "right-heavy"
		if(difference(node->left) > 0){ 	// Check if tree's right subtree is right heavy (positive balance factor)
			// Perform single left rotation (single_l)
			node = LL_rotate(node);
		}else{
			// Perform double left rotation
			node = LR_rotate(node);
		}
	}else if(balance_factor < -1){			// Check if tree is "left-heavy"
		if(difference(node->right) > 0){	// Check if tree's left subtree is right heavy (positive balance factor)
			// Perform double right rotation
			node = RL_rotate(node);
		}else{
			// Perform single right rotation (single_r)
			node = RR_rotate(node);
		}
	}
	return node;
}

/*
 * Displays AVL tree
 */
void avlTree::displayAVLtree(avl_node *ptr, int level){
	if(ptr != NULL){
		displayAVLtree(ptr->right, level+1);
		std::cout << std::endl;
		if (ptr == root){
			std::cout << "Root -> ";
		}
		for(int i=0; i< level && ptr != root; i++){
			std::cout << "        ";
		}
		std::cout << ptr->data;
		displayAVLtree(ptr->left, level+1);
	}
}

/*
 * Search the AVL tree
 */
int avlTree::searchAVLtree(int searchData){
	avl_node *curr = root;
	if (curr != NULL){
		while(curr){
			if(searchData > curr->data) curr = curr->right;
			else if (searchData < curr->data) curr = curr->left;
			else return curr->dataCounter;
		}
	}else{
		std::cout << "Tree is empty" << std::endl;
	}
	return 0;
}


#endif //_AVL_TREE_H
