/*
*	University of Southern Denmark
*	Data Structures and Algorithms
*
*	Project:		Fibonacci Series
*	Modulename:		main.cpp
*
*	Description:	Implements the Fibonacci series without the use of recursion
*
*	------------------------------------------------------
*	Change Log:
*
*	Date		ID		Change
*	YYMMDD
*
*	140905		NAL		Module Created
*	140916		NAL		Updated for multiple OS
*
*	------------------------------------------------------
*/


/***************************** Include files *******************************/
#include <iostream>
#include <cstdlib>
#include <time.h>
/*****************************    Defines    *******************************/
#ifdef _WIN32
	#define WINDOWS_OS 1;
#else
	#define WINDOWS_OS 0;
#endif

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/


int main(){
	bool Windows_OS = WINDOWS_OS;
	clock_t t;

	for (int n = 5; n < 100;){
		int first = 0, second = 1, next;
		t = clock();
		for (int i = 0; i < n; i++)
		{
			if (i <= 1)
				next = i;
			else
			{
				next = first + second;
				first = second;
				second = next;
			}
			//std::cout << next << std::endl;
		}
		t = clock() - t;
		std::cout << "Terms: " << n << "\t Takes: " << t << " clicks \t(" << ((float)t) / CLOCKS_PER_SEC << " seconds)." << std::endl;
		n += 5;
	}
	if(Windows_OS){
		system("pause");
	}
	return 0;
}
